import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import NavbarComp from "./components/NavbarComp";
import FooterComp from "./components/FooterComp";

function App() {
  return (
    <Router>
      <NavbarComp />
      <Routes>
        <Route path="/" Component={HomePage} />
      </Routes>
      <FooterComp />
    </Router>
  );
}

export default App;
