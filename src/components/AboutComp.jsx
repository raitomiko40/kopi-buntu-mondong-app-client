import React from 'react';
import LogoImg from '../assets/img/LOGO.png'

const AboutComp = () => {
  return (
    <section className="bg-light about py-5" id="about">
    {/* <h1 className="title fw-bolder text-center">TENTANG KAMI</h1> */}
    <div className="container px-4 px-lg-5 my-5">
  <div className="row justify-content-center align-items-center">
    <div className="col-2">
      <div className="about-img">
        <img className="img-about" src={LogoImg} alt="logo" />
      </div>
    </div>
    <div className="col-8">
      <div className="content">
        <h3>Kopi Buntu Mondong</h3>
        <p className='sub-about' >Sejak tahun 2019, KOPI BUNTU MONDONG tetap berusaha menghadirkan produk kopi bubuk berkualitas,
           bahan baku kopi arabika kalosi enrekang terbaik yang telah dikenal sebagai salah satu kopi terbaik 
           di Indonesia dengan rasa yang istimewa. Kopi Buntu Mondong adalah kopi bubuk 100% Arabika yang diolah
            dari biji kopi pilihan yang dihasilkan dari perkebunan milik sendiri yang luas berada di kaki gunung Latimojong,
             tepatnya di desa Buntu Mondong Kabupaten enrekang, dipadu dengan dukungan teknologi serta keingingan yang kuat 
             untuk selalu menyajikan hidangan kopi terbaik
        </p>
      </div>
    </div>
  </div>
</div>

    
</section>

  
  );
}

export default AboutComp;
