import React from 'react'

const HeaderComp = () => {
  return (
    <section className=" hero  banner-bg py-5" id='home'>
        <main className="content ">      
                <h1 className=" fw-bolder">ARABIKA KALOSI ENREKANG</h1>
                <p className=" text-white mb-3">
                Kopi Arabika Kalosi Enrekang sudah terkenal di pasar domestik dan internasional. Dikenal sebagai kopi spesial dengan rasa dan aroma yang khas.
                Memiliki berbagai aroma yang sangat khas, menggabungkan antara lain rempah - rempah, coklat, buah-buahan, bunga dan karamel
                  </p>
                <a href='#products' className='btn btn-outline-light mt-3 text-white'>Lihat Produk</a >
        </main>
    </section>
  )
}

export default HeaderComp