import React from 'react';
import Logobrand from '../assets/img/LOGO.png'

function NavbarComp() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light" id='home'>
      <div className="container px-4 px-lg-5">
        <a className="navbar-brand me-auto" href="#home">
          <img className='logo' src={Logobrand} alt="Logo" width="50" height="50" />
        </a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            <li className="nav-item"><a className="nav-link active" aria-current="page" href="#home">Home</a></li>
            <li className="nav-item"><a className="nav-link" href="#products">Produk</a></li>
            <li className="nav-item"><a className="nav-link" href="#testimoni">Testimoni</a></li>
            <li className="nav-item"><a className="nav-link" href="#about">Tentang Kami</a></li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default NavbarComp;
