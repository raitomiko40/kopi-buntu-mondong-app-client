import React, { useState } from 'react';
import productsData from '../data/products'; // Impor data produk dari file data/products.js

function ProductsComp() {
  // Fungsi untuk mengonversi angka menjadi format rupiah yang benar
  const formatRupiah = (angka) => {
    let reverse = angka.toString().split('').reverse().join('');
    let ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    return 'Rp ' + ribuan;
  };

  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [orderInfo, setOrderInfo] = useState({
    quantity: 1,
    name: '',
    address: '',
  });

  const openModal = (product) => {
    setSelectedProduct(product);
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
    setSelectedProduct(null);
    setOrderInfo({
      quantity: 1,
      name: '',
      address: '',
    });
  };

  const sendMessage = (product, orderInfo) => {
    // Ganti dengan nomor WhatsApp tujuan
    const phoneNumber = '6282158149146';

    const message = `Saya ingin memesan ${product.name} (${product.Berat}) sebanyak ${orderInfo.quantity} dengan total harga ${formatRupiah(product.Harga * orderInfo.quantity)}. Nama: ${orderInfo.name}, Alamat: ${orderInfo.address}.`;

    // Buat URL WhatsApp dengan nomor tujuan dan pesan
    const whatsappUrl = `https://api.whatsapp.com/send?phone=${phoneNumber}&text=${encodeURIComponent(message)}`;

    // Buka halaman WhatsApp dalam tab baru
    window.open(whatsappUrl, '_blank');
    closeModal();
  };

  return (
    <section className="py-5" id="products">
        <div className="title">
          <h1 className="title fw-bolder text-center">
            PRODUK
          </h1>
        </div>
      <div className="container px-4 px-lg-5 mt-5">
        <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
          {productsData.map((product, index) => (
            <div className="col mb-5" key={index}>
              <div className="card h-100">
                {product.sale && (
                  <div className="badge bg-dark text-white position-absolute" style={{ top: '0.5rem', right: '0.5rem' }}>
                    Sale
                  </div>
                )}
                <img className="card-img-top" src={product.image} alt="Product" />
                <div className="card-body p-4">
                  <div className="text-center">
                    <h5 className="fw-bolder">{product.name}</h5>
                    {product.rating && (
                      <div className="d-flex justify-content-center small text-warning mb-2">
                        {Array.from({ length: product.rating }, (_, i) => (
                          <div className="bi-star-fill" key={i}></div>
                        ))}
                      </div>
                    )}
                    <div>{formatRupiah(product.Harga)}</div>
                    <button className="btn btn-outline-dark mt-3" onClick={() => openModal(product)}>
                      Pesan Sekarang
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>

      {/* Modal */}
      {selectedProduct && (
        <div className={`modal fade ${showModal ? 'show' : ''}`} tabIndex="-1" style={{ display: showModal ? 'block' : 'none' }}>
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Pesan Produk: {selectedProduct.name}</h5>
                <button type="button" className="btn-close" onClick={closeModal}></button>
              </div>
              <div className="modal-body">
                <div className="mb-3">
                  <label htmlFor="quantity" className="form-label">Jumlah Pesanan</label>
                  <input type="number" id="quantity" className="form-control" min="1" value={orderInfo.quantity} onChange={(e) => setOrderInfo({ ...orderInfo, quantity: e.target.value })} />
                </div>
                <div className="mb-3">
                  <label htmlFor="name" className="form-label">Nama Pemesan</label>
                  <input type="text" id="name" className="form-control" value={orderInfo.name} onChange={(e) => setOrderInfo({ ...orderInfo, name: e.target.value })} />
                </div>
                <div className="mb-3">
                  <label htmlFor="address" className="form-label">Alamat Pengiriman</label>
                  <textarea id="address" className="form-control" value={orderInfo.address} onChange={(e) => setOrderInfo({ ...orderInfo, address: e.target.value })}></textarea>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" onClick={closeModal}>Tutup</button>
                <button type="button" className="btn btn-primary" onClick={() => sendMessage(selectedProduct, orderInfo)}>Kirim Pesanan</button>
              </div>
            </div>
          </div>
        </div>
      )}
    </section>
  );
}

export default ProductsComp;
