// TestimonialComp.jsx
import React from 'react';
import testimonialData from '../data/testimonialData'; // Mengimpor data testimonial

const TestiComp = () => {
  return (
    <section className="hero-bg py-5" id='testimoni'>
      <h1 className='text-center fw-bolder text-white title'>TESTIMONI</h1>
      <div className="container px-4 px-lg-5 mt-5">
        <div className="row gx-4 gx-lg-5 row-cols-1 row-cols-md-2 row-cols-xl-3 justify-content-center">
          {testimonialData.map((testimonial, index) => (
            <div className="col mb-5" key={index}>
              <div className="card h-100">
                <div className="card-body p-4">
                  <div className="text-center">
                    <p className="fw-bolder">{testimonial.name}</p>
                    <p className="text-muted">{testimonial.text}</p>
                  </div>
                </div>
                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                  <div className="text-center">
                    <img src={testimonial.avatar} alt={testimonial.name} className="rounded-circle" width="100" height="100" />
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}

export default TestiComp;
