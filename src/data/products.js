import Natural100 from '../assets/img/NATURAL 100gr RP 25.000.png';
import Natural250 from '../assets/img/NATURAL 250gr RP 55.000.png';
import Honey100 from '../assets/img/HONEY 100gr RP 23.000.png';
import Honey250 from '../assets/img/HONEY 250gr RP 50.000.png';
import Wash100 from '../assets/img/wash 100gr RP 20.000.png';
import Wash250 from '../assets/img/wash 250gr RP 45.000.png';
import Jantan250 from '../assets/img/JANTAN 250gr RP 60.000.png';
import Jantan100 from '../assets/img/JANTAN 100gr RP 28.000.png';
import Roast1000 from '../assets/img/ROAST BEAN Rp. 170.000.png';
import Roast500 from '../assets/img/ROAST BEAN Rp. 90.000.png';
import Grean from '../assets/img/GREEN BEAN RP.130.000.png';


const products = [
    {
      name: 'BUBUK NATURAL',
      image: Natural100,
      Harga: 25000,
      Berat: '100gr',
      buttonText: 'Pesan Sekarang',
    },
    {
      name: 'BUBUK NATURAL',
      image: Natural250,
      Harga: 55000,
      Berat: '250gr',
      buttonText: 'Pesan Sekarang',
      
    },
    {
      name: 'BUBUK HONEY',
      image: Honey100,
      Harga: 23000,
      Berat: '100gr',
      buttonText: 'Pesan Sekarang',
    },
    {
      name: 'BUBUK HONEY',
      image: Honey250,
      Harga: 50000,
      Berat: '250gr',
      buttonText: 'Pesan Sekarang',
      
    },
    {
      name: 'BUBUK WASH',
      image: Wash100,
      Harga: 20000,
      Berat: '100gr',
      buttonText: 'Pesan Sekarang',
    },
    {
      name: 'BUBUK WASH',
      image: Wash250,
      Harga: 45000,
      Berat: '250gr',
      buttonText: 'Pesan Sekarang',
    },
    {
      name: 'Jantan',
      image: Jantan250,
      Harga: 60000,
      Berat: '250gr',
      buttonText: 'Pesan Sekarang',
      
    },
    {
      name: 'Jantan',
      image: Jantan100,
      Harga: 28000,
      Berat: '100gr',
      buttonText: 'Pesan Sekarang',
      
    },
    {
      name: 'Roast Bean',
      image: Roast1000,
      Harga: 170000,
      Berat: '1000gr',
      buttonText: 'Pesan Sekarang',
      
    },
    {
      name: 'Roast Bean',
      image: Roast500,
      Harga: 90000,
      Berat: '500gr',
      buttonText: 'Pesan Sekarang',
      
    },
    {
      name: 'Grean Bean',
      image: Grean,
      Harga: 130000,
      Berat: '1Kg',
      buttonText: 'Pesan Sekarang',
      
    },
  ];
  
  export default products;
  