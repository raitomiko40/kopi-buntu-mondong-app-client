// testimonialData.js
const testimonialData = [
    {
      name: 'John Doe',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac justo nec quam feugiat blandit vel ac arcu.',
      avatar: 'https://dummyimage.com/100x100/dee2e6/6c757d.jpg',
    },
    {
      name: 'Jane Smith',
      text: 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
      avatar: 'https://dummyimage.com/100x100/dee2e6/6c757d.jpg',
    },
    {
      name: 'Alice Johnson',
      text: 'Vivamus quis libero eu erat tristique cursus. Fusce feugiat malesuada ligula vel convallis.',
      avatar: 'https://dummyimage.com/100x100/dee2e6/6c757d.jpg',
    },
    {
      name: 'Bob Williams',
      text: 'Suspendisse potenti. Sed sed hendrerit ex. Fusce efficitur, arcu nec malesuada interdum, justo metus malesuada purus, at vehicula ex neque auctor enim.',
      avatar: 'https://dummyimage.com/100x100/dee2e6/6c757d.jpg',
    },
  ];
  
  export default testimonialData;
  