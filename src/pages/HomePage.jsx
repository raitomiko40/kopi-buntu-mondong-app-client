import React from 'react';
import HeaderComp from '../components/HeaderComp';
import ProductsComp from '../components/ProductsComp';
import productsData from '../data/products';
import TestiComp from '../components/TestiComp';
import AboutComp from '../components/AboutComp';

const HomePage = () => {
  return (
    <>
      <HeaderComp />
      <ProductsComp products={productsData}  />
      <TestiComp />
      <AboutComp />
    </>
  );
}

export default HomePage;
